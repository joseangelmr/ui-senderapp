package gcm.play.android.samples.com.gcmquickstart.models;


public final class Device {
    public final String device_id;
    public final String token;

    public Device(String device_id, String token) {
        this.device_id = device_id;
        this.token = token;
    }
}