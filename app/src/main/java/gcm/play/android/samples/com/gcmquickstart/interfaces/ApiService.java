package gcm.play.android.samples.com.gcmquickstart.interfaces;

import java.util.HashMap;

import gcm.play.android.samples.com.gcmquickstart.models.Device;
import gcm.play.android.samples.com.gcmquickstart.models.SafeCityPhoto;
import retrofit.Callback;
import retrofit.http.Body;
import retrofit.http.Headers;
import retrofit.http.POST;

public interface ApiService {

    // endpoints
    String ENDPOINT_DEVICES = "http://52.11.22.173/api/android-devices";
    String ENDPOINT_PHOTOS = "http://52.11.22.173/api/scphotos";
    String TOKEN = "ba97a0088e21ef515301f297125d1fcedd04f85a";

    @Headers({
            "Content-Type: application/json",
            "Authorization: Token " + TOKEN
    })
    @POST("/")
    void registerDevice(@Body HashMap<String, String> arguments, Callback<Device> callback);

    @Headers({
            "Content-Type: application/json",
            "Authorization: Token " + TOKEN
    })
    @POST("/")
    void uploadPhoto(@Body HashMap<String, String> arguments, Callback<SafeCityPhoto> callback);

}
