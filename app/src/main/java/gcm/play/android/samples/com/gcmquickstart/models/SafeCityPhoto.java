package gcm.play.android.samples.com.gcmquickstart.models;


public final class SafeCityPhoto {
    public final String device_id;
    public final String name;
    public final String camera;
    public final String quality;
    public final String encode_image;

    public SafeCityPhoto(String device_id, String name, String camera,//
                         String quality, String encode_image) {
        this.device_id = device_id;
        this.name = name;
        this.camera = camera;
        this.quality = quality;
        this.encode_image = encode_image;
    }
}
