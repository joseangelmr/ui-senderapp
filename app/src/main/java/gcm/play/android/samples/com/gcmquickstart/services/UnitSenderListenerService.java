package gcm.play.android.samples.com.gcmquickstart.services;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.NotificationCompat;
import android.telephony.TelephonyManager;
import android.util.Log;

import com.google.android.gms.gcm.GcmListenerService;

import java.io.File;
import java.util.HashMap;

import gcm.play.android.samples.com.gcmquickstart.MainActivity;
import gcm.play.android.samples.com.gcmquickstart.R;
import gcm.play.android.samples.com.gcmquickstart.enums.NotificationEnum;
import gcm.play.android.samples.com.gcmquickstart.interfaces.ApiService;
import gcm.play.android.samples.com.gcmquickstart.models.SafeCityPhoto;
import gcm.play.android.samples.com.gcmquickstart.utils.ImageUtils;
import gcm.play.android.samples.com.gcmquickstart.utils.ServiceGenerator;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class UnitSenderListenerService extends GcmListenerService {

    private static final String TAG = "UnitSenderListenerService";

    /**
     * Called when message is received.
     *
     * @param from SenderID of the sender.
     * @param data Data bundle containing message data as key/value pairs.
     *             For Set of keys use data.keySet().
     */
    @Override
    public void onMessageReceived(String from, Bundle data) {
        String key = data.keySet().toArray()[0].toString();

        if (key.equals(NotificationEnum.PHOTO.toString())) {
            String photoName = data.getString(key);
            uploadHDPhoto(photoName);
        } else if (key.equals(NotificationEnum.APK.toString())) {
            // updateUnitSender(apk);
        } else if (key.equals(NotificationEnum.SETTING.toString())) {
            // setSettings(file);
        }
    }

    private void uploadHDPhoto(String photoName) {
        String directoryName = Environment.getExternalStorageDirectory() + "/247timelapse/";
        File image = ImageUtils.getInstance().findFile(photoName, directoryName);

        if (image != null) {
            HashMap<String, String> photoData = new HashMap<>();
            TelephonyManager telephonyManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
            String device_id = telephonyManager.getDeviceId();
            String name = image.getName();
            String encode_image = ImageUtils.getInstance().encodeImage(image);

            photoData.put("device_id", device_id);
            photoData.put("name", name);
            photoData.put("encode_image", encode_image);
            photoData.put("quality", "High");

            ApiService service = ServiceGenerator
                    .createService(ApiService.class, ApiService.ENDPOINT_PHOTOS);

            service.uploadPhoto(photoData, new Callback<SafeCityPhoto>() {
                @Override
                public void success(SafeCityPhoto safeCityPhoto, Response response) {
                    Log.i(TAG, response.getReason());
                }

                @Override
                public void failure(RetrofitError error) {
                    Log.e(TAG, error.getMessage());
                }
            });
        } else {
            Log.e(TAG, "HD Photo " + photoName + " not found");
        }
    }

    /**
     * Create and show a simple notification containing the received GCM message.
     *
     * @param message GCM message received.
     */
    private void sendNotification(String message) {
        Intent intent = new Intent(this, MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, intent,
                PendingIntent.FLAG_ONE_SHOT);

        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.drawable.ic_stat_ic_notification)
                .setContentTitle("GCM Message")
                .setContentText(message)
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent);

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        notificationManager.notify(0 /* ID of notification */, notificationBuilder.build());
    }
}
