package gcm.play.android.samples.com.gcmquickstart.utils;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Base64;
import android.util.Log;

import org.apache.commons.io.FileUtils;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;

public class ImageUtils {
    private static ImageUtils instance = new ImageUtils();
    private static final String TAG = "ImageUtils";

    private ImageUtils() {
    }

    public static ImageUtils getInstance() {
        if (instance == null) {
            instance = new ImageUtils();
        }
        return instance;
    }

    public void listOfFiles(String directoryName, ArrayList<File> files) {
        File directory = new File(directoryName);

        // get all the files from a directory
        File[] fList = directory.listFiles();
        for (File file : fList) {
            if (file.isFile() && isImage(file.getName())) {
                files.add(file);
            } else if (file.isDirectory()) {
                listOfFiles(file.getAbsolutePath(), files);
            }
        }

    }

    public File findFile(String photoName, String fileDirectory) {
        ArrayList<File> photos = new ArrayList<>();
        listOfFiles(fileDirectory, photos);
        for (File photo : photos) {
            if (photoName.equals(photo.getName())) {
                return photo;
            }
        }

        return null;
    }

    public String getFileExtension(String fileName) {
        return fileName.substring((fileName.lastIndexOf(".") + 1), fileName.length());
    }

    private boolean isImage(String fileName) {
        return Arrays.asList("jpeg", "jpg", "png").contains(getFileExtension(fileName));
    }

    public String encodeImage(File imageFile) {

        byte[] imageData = new byte[0];

        try {
            imageData = FileUtils.readFileToByteArray(imageFile);
        } catch (IOException e) {
            Log.e(ImageUtils.class.getName(), e.getMessage());
            e.printStackTrace();
        }

        return Base64
                .encodeToString(imageData, Base64.DEFAULT)
                .replace("\n", "");
    }

    public Dimension<Integer, Integer> getDimension(byte[] data) {
        Bitmap image = BitmapFactory.decodeByteArray(data, 0, data.length);
        int width = image.getWidth();
        int height = image.getHeight();
        return new Dimension<>(width, height);
    }

    public File compressImage(File imageFile, String directory) {
        byte[] data = new byte[0];

        try {
            data = FileUtils.readFileToByteArray(imageFile);
        } catch (IOException e) {
            Log.e(TAG, e.getMessage());
            e.printStackTrace();
        }
        Dimension dimension = getDimension(data);

        int width = (int) dimension.getWidth() / 2;
        int height = (int) dimension.getHeight() / 2;

        // Resize photo from camera byte array
        Bitmap image = BitmapFactory.decodeByteArray(data, 0, data.length);
        Bitmap imageScaled = Bitmap.createScaledBitmap(image, width, height
                * image.getHeight() / image.getWidth(), false);

        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        imageScaled.compress(Bitmap.CompressFormat.JPEG, 100, bos);

        byte[] scaledData = bos.toByteArray();
        File file = new File(directory + File.separator + imageFile.getName());
        try {
            FileUtils.writeByteArrayToFile(file, scaledData);
        } catch (IOException e) {
            Log.e(TAG, e.getMessage());
            e.printStackTrace();
        }
        return file;
    }

    private class Dimension<X, Y> {
        private X width;
        private Y height;

        public Dimension(X width, Y height) {
            this.width = width;
            this.height = height;
        }

        public X getWidth() {
            return width;
        }

        public void setWidth(X width) {
            this.width = width;
        }

        public Y getHeight() {
            return height;
        }

        public void setHeight(Y height) {
            this.height = height;
        }
    }
}