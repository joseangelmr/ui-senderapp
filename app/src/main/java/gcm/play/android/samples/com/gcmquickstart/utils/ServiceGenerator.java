package gcm.play.android.samples.com.gcmquickstart.utils;

import retrofit.RestAdapter;

public class ServiceGenerator {

    public static <S> S createService(Class<S> serviceClass, String endpoint) {
        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint(endpoint)
                .build();
        return restAdapter.create(serviceClass);
    }
}
