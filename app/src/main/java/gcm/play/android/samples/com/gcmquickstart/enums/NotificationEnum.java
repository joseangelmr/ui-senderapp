package gcm.play.android.samples.com.gcmquickstart.enums;

public enum NotificationEnum {
    PHOTO("photo_name"),
    APK("apk"),
    SETTING("settings");

    private final String notification;

    NotificationEnum(String notification) {
        this.notification = notification;
    }

    @Override
    public String toString() {
        return notification;
    }
}
