package gcm.play.android.samples.com.gcmquickstart.services;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.CountDownTimer;
import android.os.Environment;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.widget.Toast;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import gcm.play.android.samples.com.gcmquickstart.interfaces.ApiService;
import gcm.play.android.samples.com.gcmquickstart.models.SafeCityPhoto;
import gcm.play.android.samples.com.gcmquickstart.utils.ImageUtils;
import gcm.play.android.samples.com.gcmquickstart.utils.ServiceGenerator;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class UploadPhotosService extends Service {

    private static final String TAG = "UploadPhotosService";

    @Override
    public void onCreate() {
        super.onCreate();
        startCountDownTimer();
    }

    private void startCountDownTimer() {
        Toast.makeText(UploadPhotosService.this, "Upload Photos Service Started", Toast.LENGTH_SHORT).show();
        new CountDownTimer(60000, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {

            }

            public void onFinish() {
                uploadPhotos();
                this.start();
            }
        }.start();

    }

    private void uploadPhotos() {
        File lowPhotosDir = new File(Environment.getExternalStorageDirectory() + "/unitsender/low");

        if (!lowPhotosDir.isDirectory()) {
            lowPhotosDir.mkdirs();
        }

        TelephonyManager telephonyManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
        ApiService service = ServiceGenerator
                .createService(ApiService.class, ApiService.ENDPOINT_PHOTOS);

        String directoryName = Environment.getExternalStorageDirectory() + "/247timelapse/";
        List<File> photoList = new ArrayList<>();

        ImageUtils.getInstance().listOfFiles(directoryName, (ArrayList<File>) photoList);

        String device_id = telephonyManager.getDeviceId();
        String name = null;
        String encode_image = null;

        HashMap<String, String> photoData = new HashMap<>();
        photoData.put("device_id", device_id);

        for (File photo : photoList) {
            File newPhoto = ImageUtils.getInstance().compressImage(photo, lowPhotosDir.getPath());
            name = newPhoto.getName();
            encode_image = ImageUtils.getInstance().encodeImage(newPhoto);

            photoData.put("name", name);
            photoData.put("encode_image", encode_image);

            service.uploadPhoto(photoData, new Callback<SafeCityPhoto>() {
                @Override
                public void success(SafeCityPhoto safeCityPhoto, Response response) {
                    Log.i(TAG, response.getReason());
                }

                @Override
                public void failure(RetrofitError error) {
                    Log.e(TAG, error.getMessage());
                    Toast.makeText(UploadPhotosService.this, error.getMessage(), Toast.LENGTH_SHORT).show();
                }
            });
        }
    }


    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
}